# Day 6
โจทย์วันที่ 6 จะเป้นเรื่องการใช้ redux โดยมี Reducer, Action Creator เพื่อนสร้างการทำงานให้ redux ของเราสามารถส่งต่อข้อมุลได้ตามหลักการของ redux

#### Action Creator
เป็นการเตรียม Action ที่เรามีการใช้ซ้ำพร้อมเก็บโครงสร้างข้อมูลที่ส่งไปมาต่างๆเอาไว้ โดยปรกติการจะส่ง Action เราจะส่งดดยตรงผ่าน Dispatch
```(javascript)
    dispatch({
        type: 'ADD_TODO',
        payload: { topic: 'New Topic' }
    })
```
แต่เมื่อเรามีการใช้ซ้ำหลายครั้ง เราจึงสามารถดึงเฉพาะส่วน Action  ออกมาทำเป็นฟังชั่นต่างหากได้ โดยสามรถปรับการจัดเรียงได้ตามเราต้องการ
```(javascript)
    const addTodo = (payload) => ({ type: 'ADD_TODO', payload }) //ส่ง payload ทั้งหมดไปเลย
    const addTodo = ({ topic }) => ({ type: 'ADD_TODO', paylaod: { topic } }) //ส่งเฉพาะบางตัวแปรไป
    
    dispatch(addTodo('New Topic'))
```

#### Redux Middleware
Middleware เป็นระบบการทำงานที่ถูกใช้ในหลายๆเฟรมเวิร์คของจาวาสคริปต์ คอนเซปคือ "ทำงานขั้นกลาง" ตามชื่อของมัน