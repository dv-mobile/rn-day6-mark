import { connect } from 'react-redux'
import { push } from 'connected-react-router'

import { ROUTE_ITEMS } from '../constants/RouteConstants'

import LoginPage from '../pages/LoginPage'

const mapStateToProps = ({ user }) => ({
    username: user.username,
    password: user.password,
})
const mapDispatchToProps = dispatch => ({
    onSuccess: () => {
        dispatch(push(ROUTE_ITEMS))
    },
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(LoginPage)
