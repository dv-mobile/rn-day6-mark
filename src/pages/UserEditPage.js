import React from 'react'
import styled from 'styled-components'
import { connect } from 'react-redux'
import { goBack } from 'connected-react-router'

import Container from '../components/general/Container'
import {
    Body,
    Button,
    DefaultText,
    DefaultInput,
} from '../components/General.styled'

class UserEditPage extends React.Component {
    state = {
        username: '',
        firstname: '',
        lastname: '',
    }
    componentDidMount() {
        this.getDefaultValue()
    }
    getDefaultValue = () => {
        const { username, firstname, lastname } = this.props
        this.setState({ username, firstname, lastname })
    }
    onChangeValue = (index, value) => this.setState({ [index]: value })
    onSubmit = () => {
        const { onSave } = this.props
        onSave(this.state)
    }
    render() {
        const { username, firstname, lastname } = this.state
        return (
            <Container
                title={'Edit Profile'}
                hideFooter={true}
                hideGoBack={false}
            >
                <Body>
                    <DefaultInput
                        placeholder={'Username'}
                        value={username}
                        onChangeText={value =>
                            this.onChangeValue('username', value)
                        }
                    />
                    <DefaultInput
                        placeholder={'Firstname'}
                        value={firstname}
                        onChangeText={value =>
                            this.onChangeValue('firstname', value)
                        }
                    />
                    <DefaultInput
                        placeholder={'Lastname'}
                        value={lastname}
                        onChangeText={value =>
                            this.onChangeValue('lastname', value)
                        }
                    />
                </Body>
                <SaveButton onPress={this.onSubmit}>
                    <DefaultText>Save</DefaultText>
                </SaveButton>
            </Container>
        )
    }
}

const SaveButton = styled(Button)`
    justify-content: center;
    align-items: center;
`

export default connect(
    null,
    dispatch => ({
        goBack: state => dispatch(goBack()),
    })
)(UserEditPage)
