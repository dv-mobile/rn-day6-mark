import React from 'react'
import { connect } from 'react-redux'
import { goBack } from 'connected-react-router'
import {
    HeaderContainer,
    HeaderText,
    HeaderIcon,
    Center,
} from '../General.styled'

class Header extends React.Component {
    render() {
        const { title = 'Items', hideGoBack, goBack } = this.props
        return (
            <HeaderContainer>
                {hideGoBack ? null : (
                    <HeaderIcon onPress={goBack}>
                        <HeaderText>X</HeaderText>
                    </HeaderIcon>
                )}
                <Center style={{ flex: 1 }}>
                    <HeaderText>{title}</HeaderText>
                </Center>
            </HeaderContainer>
        )
    }
}

export default connect(
    null,
    { goBack }
)(Header)
