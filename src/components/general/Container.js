import React from 'react'
import Header from './Header'
import Footer from './Footer'
import { Body, Content } from '../General.styled'

class Container extends React.Component {
    render() {
        const { children, title, hideGoBack, hideFooter } = this.props
        return (
            <Body>
                <Header title={title} hideGoBack={hideGoBack} />
                <Content>{children}</Content>
                {hideFooter ? null : <Footer />}
            </Body>
        )
    }
}

export default Container
