import { ITEM_ADD, ITEM_EDIT, ITEM_REMOVE } from '../constants/AppConstants'

const DEFAULT_STATE = [
    { image: 'https://picsum.photos/200/300/?random', name: 'Big Mac' },
    { image: 'https://picsum.photos/200/300/?random', name: 'Double Cheese' },
]

const productAdd = (state, payload) => {
    const mapped = state.map(item => item)
    mapped.push(payload)
    return mapped
}

const productEdit = (state, { index, ...rest }) => {
    return state.map((item, targetIndex) =>
        targetIndex === index ? rest : item
    )
}

const productRemove = (state, { index }) => {
    return state.filter((item, targetIndex) => targetIndex !== index)
}

export default (state = DEFAULT_STATE, { type, payload }) => {
    switch (type) {
        case ITEM_ADD:
            return productAdd(state, payload)
        case ITEM_EDIT:
            return productEdit(state, payload)
        case ITEM_REMOVE:
            return productRemove(state, payload)
        default:
            return state
    }
}
